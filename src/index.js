const express = require('express');
const app = express();

// Middlewares
app.use(express.json());

// Mock
let questions = [
    {
        id: 1,
        description: '',
    },
    {
        id: 2,
        description: '',
    }
];

let answers = [
    {
        idQuestion: 2,
        idAnswer: 1,
        description: '',
        isCorrect: true,
    }
];

// Routes
app.use(require('../routes/questions'));

// Getters
app.get('/api/questions', (request, response) => {
    response.json(questions);
});

app.get('/api/answers/:idQuestion', (request, response) => {
    const idQ = Number(request.params.idQuestion);
    const answer = answers.find(answer => answer.idQuestion === idQ);
    console.log(answer);
    if (answer) {
        return response.json(answer);
    }
    else {
        response.status(404).end();
    }
});

app.get('/api/answer/correct/:idAnswer', (request, response) => {
    const idA = Number(request.params.idAnswer);
    const answer = answers.find(answer => answer.idAnswer === idA && answer.isCorrect);
    if (answer) {
        return response.json(answer);
    }
    else {
        response.status(404).end();
    }
});

// Settings
const PORT = process.env.PORT || 3002;
app.listen(PORT, () => {
    console.log('Server running on port ${PORT}')});


