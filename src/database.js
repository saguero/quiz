const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    database: 'quiz_db',
    user: 'root_quiz_db',
    password: '12345678',
    port: 3002,
});

connection.connect(function(error) {
    if(error) {
        console.log(error);
    }
    else {
        console.log('OK');
    }
});

module.exports = connection;

