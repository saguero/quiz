const express = require('express');
const router = express.Router();

 // waiting to create table questions
// Connecting to DB
const mysqlConnection = require('../src/database');

router.get('/api/questions', (request, response) => {
    mysqlConnection.query('SELECT * FROM questions', (error, rows, fields) => {
            if (!error) {
                response.json(rows);
            } else {
                console.log(error);
            }
        })
    }
);

module.exports = router;
