CREATE DATABASE IF NOT EXISTS quiz_db;

USE quiz_db;

CREATE TABLE questions (
	idQuestion INT NOT NULL auto_increment,
    description VARCHAR(50) DEFAULT NULL,
    PRIMARY KEY(idQuestion)
);

DESCRIBE questions;

CREATE TABLE answers (
	idQuestion INT NOT NULL,
    idAnswer INT NOT NULL auto_increment,
    description VARCHAR(50) DEFAULT NULL,
    isCorrect BOOLEAN DEFAULT TRUE,
    PRIMARY KEY(idAnswer),
    CONSTRAINT fk_question FOREIGN KEY(idQuestion)
    REFERENCES questions(idQuestion)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

DESCRIBE answers;
